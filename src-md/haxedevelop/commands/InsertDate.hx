package haxedevelop.commands;
import monodevelop.components.commands.*;
import monodevelop.ide.*;
import monodevelop.ide.gui.*;
import monodevelop.ide.gui.content.*;
import mono.texteditor.*;

class InsertDateHandler extends monodevelop.components.commands.CommandHandler
{
	@:protected @:overload override function Run()
	{
		var doc = IdeApp.Workbench.ActiveDocument;
		var textData = (doc.GetContent() : ITextEditorDataProvider).GetTextEditorData();
		var date = Date.now().toString();
		textData.InsertAtCaret(date);
	}

	@:protected @:overload override function Update(info:CommandInfo)
	{
		var doc = IdeApp.Workbench.ActiveDocument;
		info.Enabled = doc != null && (doc.GetContent() : ITextEditorDataProvider) != null;
	}
}

enum DateInserterCommands
{
	InsertDate;
}
