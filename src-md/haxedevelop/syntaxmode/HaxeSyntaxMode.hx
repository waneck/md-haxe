package haxedevelop.syntaxmode;
import cs.system.collections.generic.*;
import cs.Lib;
import mono.texteditor.*;
import mono.texteditor.highlighting.*;
using Lambda;

@:nativeGen
class HaxeSyntaxMode extends SyntaxMode
{
	static var baseMode:SyntaxMode;

	public function new()
	{
		super();
		var matches = new List_1();
		if (baseMode == null)
		{
			var res =
				Lib.array(Lib.toNativeType(HaxeSyntaxMode).Assembly.GetManifestResourceNames())
					.filter(function(v) return v.indexOf('HaxeSyntaxHighlightingMode') >= 0)
					[0];
			var provider = new ResourceStreamProvider(
					Lib.toNativeType(HaxeSyntaxMode).Assembly,
					res
			);
			var s = provider.Open();
			baseMode = SyntaxMode.Read(s);
			s.Close();
		}

		this.rules = new List_1(baseMode.Rules);
		this.keywords = new List_1(baseMode.Keywords);
		var spans = new cs.NativeArray(baseMode.Spans.Length),
				i = 0;
		for (s in Lib.array(baseMode.Spans))
			spans[i++] = s;
		this.spans = spans;

		matches.AddRange(cast baseMode.Matches);
		this.prevMarker = baseMode.PrevMarker;
		this.SemanticRules = new List_1(baseMode.SemanticRules);
		this.keywordTable = baseMode.keywordTable;
		this.keywordTableIgnoreCase = baseMode.keywordTableIgnoreCase;
		this.properties = baseMode.Properties;
		SetDelimiter(baseMode.GetDelimiter(null));

		this.matches = matches.ToArray();
	}
}
