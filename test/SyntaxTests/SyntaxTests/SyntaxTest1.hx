﻿/**
	The following are visual tests and should be performed on MonoDevelop
**/
class SyntaxTest1
{
	@:final @:nonstandard public function testNormalMode()
	{
		var test = ~/xyz\d\t/gi;
		if (test.match("aaaaa"))
		{
			trace('shouldnt happen ${test}');
		} else {
			trace('here\t\n\r\x00');
		}
		var number = .2;
		var num2 = 1234;
	}

	public function testPreprocessors() //aaaaa
	{
		var #if test x #else y = ~/r\[eg\]ex/ #end; //hello
		var #if test x #elseif something y = ~/r[eg]ex/ #end; //hello
		var #if   test x #elseif   something y = ~/r[eg]ex/ #end; //hello
		var #if test y #else z #end = ~/r[eg]ex/;
		var #if (x && (y || test)) y #else z #end = ~/r[eg]ex/;
		var #if (x && (y
			|| test)) y #else z #end = ~/r[eg]ex/;
		var #if 
				test
		y #else z #end = ~/r[eg]ex/;
	}

	public #if false inline #else static function testPreprocessorInKeyword()
	{
		#if false
		agfkljd;gflkadjg;lfdkjgafl;dkjgfadlkgfjdalgkfj
		#else
		var xx = "test";
		#end
	}
}
